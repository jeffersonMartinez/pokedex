//
//  Colors.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation
import UIKit

enum CustomColor: String {
    case mainText = "MainText"
    case secondText = "SecondText"
    case blue1 = "Blue1"
    case blue2 = "Blue2"
}

extension UIColor {
    static func setColor(_ color: CustomColor) -> UIColor {
        return UIColor(named: color.rawValue)!
    }
}
