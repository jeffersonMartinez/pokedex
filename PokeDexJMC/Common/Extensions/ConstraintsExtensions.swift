//
//  ConstraintsExtensions.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 7/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func constraintToMarginParent(parent: UIView,
                                  top: CGFloat = 0, leading: CGFloat = 0, bottom: CGFloat = 0, trailing: CGFloat = 0,
                                  activateTop: Bool = true, activateLeading: Bool = true, activateBottom: Bool = true, activateTrailing: Bool = true) {
        self.topAnchor.constraint(equalTo: parent.safeAreaLayoutGuide.topAnchor, constant: top).isActive = activateTop
        self.leadingAnchor.constraint(equalTo: parent.leadingAnchor, constant: leading).isActive = activateLeading
        self.bottomAnchor.constraint(equalTo: parent.bottomAnchor, constant: bottom).isActive = activateBottom
        self.trailingAnchor.constraint(equalTo: parent.trailingAnchor, constant: trailing).isActive = activateTrailing
    }
    
    func constraintToTopLeading(parent: UIView, top: CGFloat = 0, leading: CGFloat = 0) {
        self.topAnchor.constraint(equalTo: parent.topAnchor, constant: top).isActive = true
        self.leadingAnchor.constraint(equalTo: parent.leadingAnchor, constant: leading).isActive = true
    }
    
    func constraintOnlyHeightWidth(width: CGFloat, height: CGFloat) {
        self.widthAnchor.constraint(equalToConstant: width).isActive = true
        self.heightAnchor.constraint(equalToConstant: height).isActive = true
    }
    
    func constraintTopCenterX(parent: UIView, top: CGFloat = 0, x: CGFloat = 0,
                              activateTop: Bool = true, activateX: Bool = true) {
        self.topAnchor.constraint(equalTo: parent.topAnchor, constant: top).isActive = activateTop
        self.centerXAnchor.constraint(equalTo: parent.centerXAnchor, constant: x).isActive = activateX
    }
}
