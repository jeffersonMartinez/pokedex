//
//  ViewExtensions.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 7/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func round(_ radius: CGFloat) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = radius
    }
    
}
