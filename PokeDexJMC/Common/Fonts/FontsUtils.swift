//
//  FontUtil.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation
import UIKit

enum FontStyle: String {
    case avenirBook = "AvenirLTStd-Book"
    case avenirRoman = "AvenirLTStd-Roman"
    case avenirMedium = "AvenirLTStd-Medium"
}

extension UIFont {
    static func mainFont(size: CGFloat = 20) -> UIFont? {
        return UIFont(name: FontStyle.avenirMedium.rawValue, size: size)
    }
    
    static func secondFont(size: CGFloat = 15) -> UIFont? {
        return UIFont(name: FontStyle.avenirBook.rawValue, size: size)
    }
    
}
