//
//  LocalizableExtension.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
