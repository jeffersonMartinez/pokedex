//
//  TextConstants.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation

struct TextConstants {
    
    // MARK : text for MainViewController
    static let firstVCTitle = "pokemon".localized
    static let secondVCTitle = "moves".localized
    static let thirdVCTitle = "item".localized
    
    // MARK: text for DetailViewController
    static let detailControlOne = "stats".localized.uppercased()
    static let detailControlTwo = "evolutions".localized.uppercased()
    static let detailControlThree = "moves".localized.uppercased()
    
    
}
