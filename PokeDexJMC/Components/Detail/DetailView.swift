//
//  DetailView.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation
import UIKit

extension DetailViewController {

    func setupView() {
        view.backgroundColor = .white
        
        scvContainer.translatesAutoresizingMaskIntoConstraints = false
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        gradientLayer.colors = [UIColor.setColor(.blue1).cgColor, UIColor.setColor(.blue2).cgColor]
        
        scvContainer.layer.insertSublayer(gradientLayer, at: 0)
        
        ivClose.translatesAutoresizingMaskIntoConstraints = false
        //ivClose.image = UIImage(named: "arrow_down")?.withRenderingMode(.alwaysTemplate)
        ivClose.image = #imageLiteral(resourceName: "arrow_down").withRenderingMode(.alwaysTemplate)
        ivClose.tintColor = .white
        ivClose.contentMode = .scaleAspectFit
        
        viewContainer.translatesAutoresizingMaskIntoConstraints = false
        viewContainer.backgroundColor = .white
        viewContainer.round(50)
        
        ivPhoto.translatesAutoresizingMaskIntoConstraints = false
        ivPhoto.contentMode = .scaleToFill
        
        lblName.translatesAutoresizingMaskIntoConstraints = false
        lblName.font = UIFont.mainFont(size: 40)
        lblName.textColor = UIColor.setColor(.mainText)
        
        ivType1.translatesAutoresizingMaskIntoConstraints = false
        ivType1.contentMode = .scaleAspectFit
    
        
        lblDescription.translatesAutoresizingMaskIntoConstraints = false
        lblDescription.font = UIFont.secondFont()
        lblDescription.textColor = UIColor.setColor(.secondText)
        lblDescription.numberOfLines = 0
        lblDescription.textAlignment = .center
        lblDescription.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pretium sit amet sapien nec varius. Aliquam risus dui, varius quis ligula in, porttitor porttitor dolor. Integer felis est, egestas a egestas nec, rhoncus nec sapien. Nulla elementum ante neque, in rutrum nisi feugiat et. Donec quis cursus ligula, a posuere metus. Aliquam malesuada quam at elit tincidunt, sed mattis erat porttitor."
        
        sgcDetail.translatesAutoresizingMaskIntoConstraints = false
        sgcDetail.insertSegment(withTitle: TextConstants.detailControlOne, at: 0, animated: true)
        sgcDetail.insertSegment(withTitle: TextConstants.detailControlTwo, at: 1, animated: true)
        sgcDetail.insertSegment(withTitle: TextConstants.detailControlThree, at: 2, animated: true)
        sgcDetail.selectedSegmentIndex = 0
        
        viewControl.translatesAutoresizingMaskIntoConstraints = false
        
        
        view.addSubview(scvContainer)
        scvContainer.addSubview(ivClose)
        scvContainer.addSubview(viewContainer)
        scvContainer.addSubview(ivPhoto)
        viewContainer.addSubview(lblName)
        viewContainer.addSubview(ivType1)
        viewContainer.addSubview(lblDescription)
        viewContainer.addSubview(sgcDetail)
        viewContainer.addSubview(viewControl)
        
        setupConstrains()
    }
    
    private func setupConstrains() {
        scvContainer.constraintToMarginParent(parent: view, activateTop: false)
        scvContainer.topAnchor.constraint(equalTo: view.topAnchor).isActive = true

        ivClose.constraintOnlyHeightWidth(width: 25, height: 25)
        ivClose.constraintToTopLeading(parent: scvContainer, top: 70, leading: 10)
        
        ivPhoto.constraintOnlyHeightWidth(width: 170, height: 170)
        ivPhoto.constraintTopCenterX(parent: scvContainer, top: 88)
        
        viewContainer.constraintToMarginParent(parent: view, activateTop: false, activateBottom: false)
        viewContainer.topAnchor.constraint(equalTo: scvContainer.topAnchor, constant: 220).isActive = true
        viewContainer.bottomAnchor.constraint(equalTo: scvContainer.bottomAnchor).isActive = true
        
        lblName.constraintTopCenterX(parent: viewContainer, top: 70)
        
        ivType1.topAnchor.constraint(equalTo: lblName.bottomAnchor, constant: 20).isActive = true
        ivType1.centerXAnchor.constraint(equalTo: viewContainer.centerXAnchor).isActive = true
        ivType1.constraintOnlyHeightWidth(width: 120, height: 40)
        
        lblDescription.constraintToMarginParent(parent: viewContainer, leading: 20, trailing: -20, activateTop: false, activateBottom: false)
        lblDescription.topAnchor.constraint(equalTo: ivType1.bottomAnchor, constant: 20).isActive = true
        
        sgcDetail.topAnchor.constraint(equalTo: lblDescription.bottomAnchor, constant: 20).isActive = true
        sgcDetail.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        sgcDetail.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        
        viewControl.topAnchor.constraint(equalTo: sgcDetail.bottomAnchor, constant: 20).isActive = true
        viewControl.leadingAnchor.constraint(equalTo: sgcDetail.leadingAnchor).isActive = true
        viewControl.trailingAnchor.constraint(equalTo: sgcDetail.trailingAnchor).isActive = true
        viewControl.bottomAnchor.constraint(equalTo: viewContainer.bottomAnchor).isActive = true
    
        
        viewControl.heightAnchor.constraint(equalToConstant: 300).isActive = true
        
    }
}
