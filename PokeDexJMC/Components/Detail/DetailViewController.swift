//
//  DetailViewController.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    let scvContainer = UIScrollView()
    let ivClose = UIImageView()
    let viewContainer = UIView()
    let ivPhoto = UIImageView()
    let lblName = UILabel()
    let ivType1 = UIImageView()
    let lblDescription = UILabel()
    
    let sgcDetail = UISegmentedControl()
    let viewControl = UIView()
    
    private var pokemon: Pokemon?
    
    let statVC = StatsViewController()
    let moveVC = MovesViewController()
    let evoVC = EvolutionViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        eventTaps()
        showSubView(index: 0)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.navigationController?.isNavigationBarHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        DispatchQueue.main.async {
            self.navigationController?.isNavigationBarHidden = true
        }
    }
    
    func setData(pokemon: Pokemon, image: UIImage?) {
        self.pokemon = pokemon
        
        ivPhoto.image = image
        lblName.text = pokemon.name
        ivType1.image = UIImage(named: "T\(pokemon.types[0].type.name)")
        
    }
    
    func eventTaps() {
        let tapClose = UITapGestureRecognizer(target: self, action: #selector(tapClose(_:)))
        ivClose.isUserInteractionEnabled = true
        ivClose.addGestureRecognizer(tapClose)
        
        sgcDetail.addTarget(self, action: #selector(tapControl(_:)), for: .valueChanged)
    }
    
    @objc func tapClose(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)    
    }
    
    @objc func tapControl(_ sender: UISegmentedControl) {
        showSubView(index: sender.selectedSegmentIndex)
    }
    
    private func showSubView(index: Int) {
        switch index {
        case 0:
            viewControl.addSubview(statVC.view)
            statVC.didMove(toParent: self)
            if let stats = self.pokemon?.stats {
                statVC.setData(stats: stats)
            }
        case 1:
            viewControl.addSubview(evoVC.view)
            evoVC.didMove(toParent: self)
        case 2:
            moveVC.fromDetail = true
            viewControl.addSubview(moveVC.view)
            moveVC.didMove(toParent: self)
            if let moves = self.pokemon?.moves {
                moveVC.setData(moves: moves)
            }
        default:
            return
        }
    }
}
