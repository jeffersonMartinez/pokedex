//
//  EvolutionViewController.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 7/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import UIKit

class EvolutionViewController: UIViewController {

    let lblName = UILabel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        lblName.translatesAutoresizingMaskIntoConstraints = false
        lblName.text = "evolutions".localized.uppercased()
        
        view.addSubview(lblName)
        
        lblName.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lblName.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
    }
    
}
