//
//  ItemView.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation
import UIKit

extension ItemViewController {
    
    func setupView() {
        view.backgroundColor = UIColor.setColor(.blue2)
    }
    
}
