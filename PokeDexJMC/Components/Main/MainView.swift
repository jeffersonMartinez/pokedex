//
//  MainView.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation
import UIKit

extension MainViewController {
    
    func setupView() {
        let pikachuRefactor =  #imageLiteral(resourceName: "pikachu").af_imageAspectScaled(toFill: CGSize(width: 30, height: 30))
        firstVC.tabBarItem = UITabBarItem(title: TextConstants.firstVCTitle, image: pikachuRefactor, tag: 0)
        secondVC.tabBarItem = UITabBarItem(title: TextConstants.secondVCTitle, image: #imageLiteral(resourceName: "oval"), tag: 1)
        thirdVC.tabBarItem = UITabBarItem(title: TextConstants.thirdVCTitle, image: #imageLiteral(resourceName: "candy"), tag: 2)        
        
        viewControllers = [firstVC, secondVC, thirdVC]
        
        tabBar.tintColor = .black
    }
    
}
