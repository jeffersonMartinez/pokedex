//
//  MainViewController.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {

    let firstVC = PokemonViewController()
    let secondVC = MovesViewController()
    let thirdVC = ItemViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        
    }
}
