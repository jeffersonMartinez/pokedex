//
//  MovesView.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation

extension MovesViewController {
    
    func setupView() {
        view.backgroundColor = .white
        
        tvMoves.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(tvMoves)
        
        if fromDetail {
            tvMoves.constraintToMarginParent(parent: view, trailing: -30)
        } else {
            tvMoves.constraintToMarginParent(parent: view, leading: 20, trailing: -20)
        }
    }
}
