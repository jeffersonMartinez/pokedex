//
//  MovesViewController.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import UIKit

class MovesViewController: UIViewController {

    private var moveVM: MovesViewModel? = nil
    
    var fromDetail = false
    let tvMoves = UITableView()
    let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupTableView()
    }

    func setupTableView() {
        tvMoves.dataSource = self
        tvMoves.delegate = self
        tvMoves.register(PokemonTableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    func setData(moves: [Move]) {
        moveVM = MovesViewModel(moves: moves)
        DispatchQueue.main.async {
            self.tvMoves.reloadData()
        }
    }
}

extension MovesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moveVM?.numberOfRowsInSections(section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! PokemonTableViewCell
        
        guard let data = moveVM?.dataAtIndex(indexPath.row) else {
            return cell
        }
        cell.ivPhoto.isHidden = true
        cell.lblNumber.isHidden = !fromDetail
        if let name = data.move?.name {
            cell.lblName.text = name
        }
        if let version = data.versionGroupDetails?[0],
            let level = version.levelLearnedAt {
            cell.lblNumber.text = "Level \(level)"
        }
        
        
        return cell
    }
}

extension MovesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
