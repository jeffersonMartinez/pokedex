//
//  MovesViewModel.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 7/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation

struct MovesViewModel {
    let moveList: [Move]
    
    init(moves: [Move]) {
        self.moveList = moves
    }
}

extension MovesViewModel {
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRowsInSections(_ section: Int) -> Int {
        return self.moveList.count
    }
    
    func dataAtIndex(_ index: Int) -> Move {
        return self.moveList[index]
    }
}

