//
//  ViewController.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 5/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import UIKit

class PokemonViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    private var pokemonsListVM = PokemonListViewModel()
    private let cellId = "cellId"
    
    let tvPokemon = UITableView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        self.setupTableView()
        
        self.getData()
    }
    
    func setupTableView() {
        tvPokemon.dataSource = self
        tvPokemon.delegate = self
        tvPokemon.register(PokemonTableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    func getData() {
        pokemonsListVM.loadData() {
            self.view.makeToastActivity(.center)
            DispatchQueue.main.async {
                self.tvPokemon.reloadData()
                self.view.hideToastActivity()
            }
        }
    }


}

// MARK: - implements methods for dataSource
extension PokemonViewController {
    func numberOfSections(in tableView: UITableView) -> Int {
        return pokemonsListVM.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemonsListVM.numberOfRowsInSections(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! PokemonTableViewCell
        let data = pokemonsListVM.pokemonAtIndex(indexPath.row)
        
        cell.indicator.startAnimating()
        
        pokemonsListVM.loadDetailData(url: data.url) { (pokemon) in
            cell.pokemon = pokemon
            cell.lblName.text = pokemon.name
            cell.lblNumber.text = "#\(pokemon.id)"
            cell.ivType1.image = UIImage(named: pokemon.types[0].type.name)
            if pokemon.types.count > 1 {
                cell.ivType2.image = UIImage(named: pokemon.types[1].type.name)
            }
            self.pokemonsListVM.loadImage(url: pokemon.sprites.frontDefault, competion: { (image) in
                cell.ivPhoto.image = image
                cell.indicator.stopAnimating()
            })
        }        

        return cell
    }
}

// MARK: - implements methods for deletage
extension PokemonViewController {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? PokemonTableViewCell,
            let pokemon = cell.pokemon {
            pokemonsListVM.didSelect(target: self.parent!, pokemon: pokemon, image: cell.ivPhoto.image)
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}
