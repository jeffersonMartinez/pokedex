//
//  PokemonTableViewCell.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {
    
    let ivPhoto = UIImageView()
    let lblName = UILabel()
    let lblNumber = UILabel()
    let indicator = UIActivityIndicatorView(style: .whiteLarge)
    let ivType1 = UIImageView()
    let ivType2 = UIImageView()
    
    var pokemon: Pokemon?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.backgroundColor = UIColor.setColor(.secondText)
        
        ivPhoto.translatesAutoresizingMaskIntoConstraints = false
        
        lblName.translatesAutoresizingMaskIntoConstraints = false
        lblName.font = UIFont.mainFont()
        lblName.textColor = UIColor.setColor(.mainText)
        
        lblNumber.translatesAutoresizingMaskIntoConstraints = false
        lblNumber.font = UIFont.secondFont()
        lblNumber.textColor = UIColor.setColor(.secondText)
        
        ivType1.translatesAutoresizingMaskIntoConstraints = false
        ivType2.translatesAutoresizingMaskIntoConstraints = false
        
        
        addSubview(ivPhoto)
        addSubview(lblName)
        addSubview(lblNumber)
        addSubview(indicator)
        addSubview(ivType1)
        addSubview(ivType2)
        
        
        ivPhoto.heightAnchor.constraint(equalToConstant: 50).isActive = true
        ivPhoto.widthAnchor.constraint(equalTo: ivPhoto.heightAnchor).isActive = true
        ivPhoto.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        ivPhoto.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20).isActive = true
        
        lblName.topAnchor.constraint(equalTo: ivPhoto.topAnchor).isActive = true
        lblName.leadingAnchor.constraint(equalTo: ivPhoto.trailingAnchor, constant: 10).isActive = true
        
        lblNumber.topAnchor.constraint(equalTo: lblName.bottomAnchor, constant: 5).isActive = true
        lblNumber.leadingAnchor.constraint(equalTo: lblName.leadingAnchor).isActive = true
        
        indicator.centerXAnchor.constraint(equalTo: ivPhoto.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: ivPhoto.centerYAnchor).isActive = true
        
        ivType1.heightAnchor.constraint(equalToConstant: 50).isActive = true
        ivType1.widthAnchor.constraint(equalTo: ivType1.heightAnchor).isActive = true
        ivType1.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        ivType1.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        
        ivType2.heightAnchor.constraint(equalTo: ivType1.heightAnchor).isActive = true
        ivType2.widthAnchor.constraint(equalTo: ivType2.heightAnchor).isActive = true
        ivType2.trailingAnchor.constraint(equalTo: ivType1.leadingAnchor).isActive = true
        ivType2.centerYAnchor.constraint(equalTo: ivType1.centerYAnchor).isActive = true
    }

}
