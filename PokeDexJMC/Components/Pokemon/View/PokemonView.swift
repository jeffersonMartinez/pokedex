//
//  HomeView.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation
import UIKit

extension PokemonViewController {

    func setupView() {
        view.backgroundColor = .white
        
        tvPokemon.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(tvPokemon)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        
        if #available(iOS 11, *) {
            tvPokemon.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            tvPokemon.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
        }
        tvPokemon.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tvPokemon.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tvPokemon.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
    }
}
