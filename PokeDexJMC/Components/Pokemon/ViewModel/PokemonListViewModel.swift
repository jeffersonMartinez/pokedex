//
//  PokemonListViewModel.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation
import UIKit

// MARK: PokemonListViewModel
class PokemonListViewModel {
    var pokemonResults: [PokeResults] = [PokeResults]()
}

extension PokemonListViewModel {
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRowsInSections(_ section: Int) -> Int {
        return self.pokemonResults.count
    }
    
    func pokemonAtIndex(_ index: Int) -> PokemonViewModel {
        let pokemon = self.pokemonResults[index]
        return PokemonViewModel(pokemon)
    }
    
    func loadData(competion: @escaping() -> Void) {
        WebService.getRequest(url: ApiConstants.getUrlResults(), type: PokeResponse.self) { (data) in
            if let pokemons = data?.results {
                self.pokemonResults = pokemons
                competion()
            }
        }
    }
    
    func loadDetailData(url: String, competion: @escaping(Pokemon) -> Void) {
        WebService.getRequest(url: url, type: Pokemon.self) { (data) in
            if let pokemon = data {
                competion(pokemon)
            }
        }
    }
    
    func loadImage(url: String, competion: @escaping(UIImage) -> Void) {
        WebService.getRequestImage(url: url) { (image) in
            competion(image)
        }
    }
    
    func didSelect(target: UIViewController, pokemon: Pokemon, image: UIImage?) {
        let vc = DetailViewController()
        vc.modalPresentationStyle = .fullScreen
        vc.setData(pokemon: pokemon, image: image)
        target.present(vc, animated: true, completion: nil)
    }
}
