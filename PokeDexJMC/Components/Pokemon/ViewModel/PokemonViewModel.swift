//
//  PokemonViewModel.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation

// MARK: PokemonViewModel
struct PokemonViewModel {
    private let pokeResults: PokeResults
}

extension PokemonViewModel {
    init(_ pokeResults: PokeResults) {
        self.pokeResults = pokeResults
    }
}

extension PokemonViewModel {
    var name: String {
        return self.pokeResults.name
    }
    
    var url: String {
        return self.pokeResults.url
    }
}


