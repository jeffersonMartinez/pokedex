//
//  StatViewModel.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 7/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation

struct StatViewModel {
    let statList: [Stat]
    
    init(stats: [Stat]) {
        self.statList = stats
    }
}

extension StatViewModel {
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRowsInSections(_ section: Int) -> Int {
        return self.statList.count
    }
    
    func dataAtIndex(_ index: Int) -> Stat {
        return self.statList[index]
    }
}

extension StatViewModel {
    func convertAttack(name: statOrigin) -> String {
        switch name {
        case .hp:
            return statConverted.hp.rawValue
        case .attack:
            return statConverted.attack.rawValue
        case .defense:
            return statConverted.defense.rawValue
        case .specialAttack:
            return statConverted.specialAttack.rawValue
        case .specialDefense:
            return statConverted.specialDefense.rawValue
        case .speed:
            return statConverted.speed.rawValue
        }
    }
}


enum statOrigin: String {
    case hp
    case attack
    case defense
    case specialAttack = "special-attack"
    case specialDefense = "special-defense"
    case speed
}

enum statConverted: String {
    case hp = "HP"
    case attack = "ATK"
    case defense = "DEF"
    case specialAttack = "SATK"
    case specialDefense = "SDEF"
    case speed = "SPD"
}
