//
//  StatsViewController.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 7/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import UIKit

class StatsViewController: UIViewController {

    let tvStats = UITableView()
    let cellId = "cellId"
    
    private var statVM: StatViewModel? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupTableView()
    }
    
    func setupTableView() {
        tvStats.dataSource = self
        tvStats.register(StatTableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    func setData(stats: [Stat]) {
        statVM = StatViewModel(stats: stats)
        DispatchQueue.main.async {
            self.tvStats.reloadData()
        }
    }
}

extension StatsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statVM?.numberOfRowsInSections(section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! StatTableViewCell
        
        guard let data = statVM?.dataAtIndex(indexPath.row) else {
            return cell
        }
        
        if let name = data.stat?.name {
            cell.lblName.text = statVM?.convertAttack(name: statOrigin(rawValue: name) ?? .hp)
        }
        if let base = data.baseStat {
            cell.lblValue.text = "\(base)"
            cell.pgValue.progress = Float(base) / 100
        }
                
        return cell
    }
}
