//
//  StatTableViewCell.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 7/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import UIKit

class StatTableViewCell: UITableViewCell {
    
    let lblName = UILabel()
    let lblValue = UILabel()
    let pgValue = UIProgressView()

    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        
        lblName.translatesAutoresizingMaskIntoConstraints = false
        lblName.textColor = UIColor.setColor(.blue1)
        lblName.font = UIFont.secondFont(size: 14)
        
        lblValue.translatesAutoresizingMaskIntoConstraints = false
        lblValue.textColor = UIColor.setColor(.secondText)
        lblValue.font = UIFont.secondFont(size: 14)
        
        pgValue.translatesAutoresizingMaskIntoConstraints = false
        pgValue.progressTintColor = UIColor.setColor(.blue1)
        pgValue.round(5)
    
        
        addSubview(lblName)
        addSubview(lblValue)
        addSubview(pgValue)
        
        
        lblName.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        lblName.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20).isActive = true
        lblName.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        lblValue.centerYAnchor.constraint(equalTo: lblName.centerYAnchor).isActive = true
        lblValue.leadingAnchor.constraint(equalTo: lblName.trailingAnchor, constant: 10).isActive = true
        lblValue.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        pgValue.centerYAnchor.constraint(equalTo: lblName.centerYAnchor).isActive = true
        pgValue.leadingAnchor.constraint(equalTo: lblValue.trailingAnchor, constant: 10).isActive = true
        pgValue.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        pgValue.heightAnchor.constraint(equalToConstant: 10).isActive = true
        
    }

}
