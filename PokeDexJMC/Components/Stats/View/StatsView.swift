//
//  StatsView.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 7/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation
import UIKit

extension StatsViewController {
    
    func setupView() {
        
        tvStats.translatesAutoresizingMaskIntoConstraints = false
        tvStats.separatorStyle = .none
        
        view.addSubview(tvStats)
        
        tvStats.constraintToMarginParent(parent: view, activateTop: false, activateTrailing: false)
        tvStats.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tvStats.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -400).isActive = true
        
    }
    
}
