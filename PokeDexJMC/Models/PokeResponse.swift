//
//  PokeList.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation

struct PokeResponse: Codable {
    let count: Int
    let next: String
    let previous: String
    let results: [PokeResults]
    
    enum CodingKeys: String, CodingKey {
        case count = "count"
        case next = "next"
        case previous = "previous"
        case results = "results"
    }
    
    init(count: Int, next: String, previous: String, results: [PokeResults]) {
        self.count = count
        self.next = next
        self.previous = previous
        self.results = results
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        count = try values.decode(Int.self, forKey: .count)
        next = try values.decode(String.self, forKey: .next)
        previous = try values.decode(String.self, forKey: .previous)
        results = try values.decode([PokeResults].self, forKey: .results)
    }
}
