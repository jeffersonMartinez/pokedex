//
//  Pokemon.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation

//MARK: Pokemon
struct Pokemon: Codable {
    let id: Int
    let sprites: Sprite
    let species: Species
    let types: [TypeElement]
    let abilities: [Ability]
    let baseExperience: Int
    let forms: [Species]
    let gameIndices: [GameIndex]
    let height: Int
    let isDefault: Bool
    let locationAreaEncounters: String
    let moves: [Move]
    let name: String
    let order: Int
    let stats: [Stat]
    let weight: Int
    
    enum CodingKeys: String, CodingKey {
        case sprites = "sprites"
        case id = "id"
        case species = "species"
        case types = "types"
        case abilities
        case baseExperience = "base_experience"
        case forms
        case gameIndices = "game_indices"
        case height
        case isDefault = "is_default"
        case locationAreaEncounters = "location_area_encounters"
        case moves, name, order, stats, weight
    }

    init(sprites: Sprite, id: Int, species: Species, types: [TypeElement],
         abilities: [Ability], baseExperience: Int, forms: [Species], gameIndices: [GameIndex],
         height: Int, isDefault: Bool, locationAreaEncounters: String, moves: [Move], name: String,
         order: Int, stats: [Stat], weight: Int
         ) {
        self.sprites = sprites
        self.id = id
        self.species = species
        self.types = types
        self.abilities = abilities
        self.baseExperience = baseExperience
        self.forms = forms
        self.gameIndices = gameIndices
        self.height = height
        self.isDefault = isDefault
        self.locationAreaEncounters = locationAreaEncounters
        self.moves = moves
        self.name = name
        self.order = order
        self.stats = stats
        self.weight = weight
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        sprites = try values.decode(Sprite.self, forKey: .sprites)
        id = try values.decode(Int.self, forKey: .id)
        species = try values.decode(Species.self, forKey: .species)
        types = try values.decode([TypeElement].self, forKey: .types)
        abilities = try values.decode([Ability].self, forKey: .forms)
        baseExperience = try values.decode(Int.self, forKey: .baseExperience)
        forms = try values.decode([Species].self, forKey: .forms)
        gameIndices = try values.decode([GameIndex].self, forKey: .gameIndices)
        height = try values.decode(Int.self, forKey: .height)
        isDefault = try values.decode(Bool.self, forKey: .isDefault)
        locationAreaEncounters = try values.decode(String.self, forKey: .locationAreaEncounters)
        moves = try values.decode([Move].self, forKey: .moves)
        name = try values.decode(String.self, forKey: .name)
        order = try values.decode(Int.self, forKey: .order)
        stats = try values.decode([Stat].self, forKey: .stats)
        weight = try values.decode(Int.self, forKey: .weight)
    }
    
}

//MARK: Sprinte
struct Sprite: Codable {
    let frontDefault: String
    
    enum CodingKeys: String, CodingKey {
        case frontDefault = "front_default"
    }
    
    init(frontDefault: String) {
        self.frontDefault = frontDefault
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        frontDefault = try values.decode(String.self, forKey: .frontDefault)
    }
}

// MARK: - TypeElement
struct TypeElement: Codable {
    let slot: Int
    let type: Species
}

// MARK: - Species
struct Species: Codable {
    let name: String
    let url: String
}

// MARK: - Ability
struct Ability: Codable {
    let ability: Species?
    let isHidden: Bool?
    let slot: Int?
    
    enum CodingKeys: String, CodingKey {
        case ability
        case isHidden = "is_hidden"
        case slot
    }
}

// MARK: - GameIndex
struct GameIndex: Codable {
    let gameIndex: Int?
    let version: Species?
    
    enum CodingKeys: String, CodingKey {
        case gameIndex = "game_index"
        case version
    }
}

// MARK: - Move
struct Move: Codable {
    let move: Species?
    let versionGroupDetails: [VersionGroupDetail]?
    
    enum CodingKeys: String, CodingKey {
        case move
        case versionGroupDetails = "version_group_details"
    }
}

// MARK: - VersionGroupDetail
struct VersionGroupDetail: Codable {
    let levelLearnedAt: Int?
    let moveLearnMethod, versionGroup: Species?
    
    enum CodingKeys: String, CodingKey {
        case levelLearnedAt = "level_learned_at"
        case moveLearnMethod = "move_learn_method"
        case versionGroup = "version_group"
    }
}

// MARK: - Stat
struct Stat: Codable {
    let baseStat, effort: Int?
    let stat: Species?
    
    enum CodingKeys: String, CodingKey {
        case baseStat = "base_stat"
        case effort, stat
    }
}
