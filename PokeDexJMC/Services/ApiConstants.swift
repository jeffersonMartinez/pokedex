//
//  Constants.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 6/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation

struct ApiConstants {
    private static let urlBase = "https://pokeapi.co/api/v2/"
    
    static func getUrlResults() -> String {
        return "\(urlBase)pokemon?limit=100&offset=200"
    }
}
