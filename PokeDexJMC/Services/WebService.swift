//
//  WebService.swift
//  PokeDexJMC
//
//  Created by Jefferson Martinez on 5/06/20.
//  Copyright © 2020 Jefferson Martinez. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

struct WebService {
    
    static func getRequest<T: Codable>(url urlString: String, type typeResponse: T.Type, completion: @escaping (T?) -> Void) {
        guard let url = URL(string: urlString) else {
            return
        }
        
        Alamofire.request(url).responseData { response in
            if let error = response.error {
                printError(url: urlString, detail: "ERROR REPONSE: \(error)")
                completion(nil)
                return
            }
            
            guard let data = response.data else {
                printError(url: urlString, detail: "RESPONSE WITHOUT DATA")
                completion(nil)
                return
            }
            
            do {
                let convert = try JSONDecoder().decode(typeResponse, from: data)
                completion(convert)
            } catch {
                printError(url: urlString, detail: "ERROR CONVERT: \(error)")
            }
        }
    }
    
    static func getRequestImage(url urlString: String, completion: @escaping (Image) -> Void) {
        Alamofire.request(urlString).responseImage { (response) in
            if case .success(let image) = response.result {
                let size = CGSize(width: 100, height: 100)
                let imageScaled = image.af_imageScaled(to: size)
                completion(imageScaled)
            }
        }
    }
    
    private static func printError(url: String, detail: String) {
        print("URL: \(url) -\n\(detail)")
    }
}
