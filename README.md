# PokeDexJMC

Aplicación desarrollada par iOS con Xcode 10.
Esta fue construída completamente en código sin usar **storyboard** con el fin de darle más flexibilidad y demostrar un mayor manejo de las propiedades de configuración de los elementos de la **UI**
# Pantallas
  - **Main** - contiene el tabbar de navegación
  - **Pokemon** - contiene la lista de los pokemons
  - **Detail** - contiene el detalle del pokemon, este contiene 3 subvistas, las cuales son stats, evolutions y moves
 
 